package com.example.projecta.dto;

import lombok.Value;

@Value
public class Person {
	String name;
	Integer age;
}
