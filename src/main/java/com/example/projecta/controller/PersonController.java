package com.example.projecta.controller;

import com.example.projecta.dto.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class PersonController {
	@GetMapping("person")
	public Mono<Person> person() {
		return Mono.just(new Person("Ilya", 27));
	}

	@GetMapping("other")
	public Mono<Person> other() {
		return Mono.just(new Person("Ivan", 22));
	}
}
